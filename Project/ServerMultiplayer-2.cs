﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;


namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {
                
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                int num = 1;
                String c = String.Empty;
                while (true)
                {
                    string name = reader.ReadLine();
                    Console.WriteLine("Client Name : {0}", name);

                    writer.WriteLine("1. Play");
                    writer.Flush();
                    writer.WriteLine("2. Leaderboard");
                    writer.Flush();
                    writer.WriteLine("3. Exit");
                    writer.Flush();

                    string open = reader.ReadLine();
                    Console.WriteLine("Your Option : {0}", open);

                    if (open == "Play" || open == "1")
                    {
                        Game();
                    }
                    else if (open == "Leaderboard" || open == "2")
                    {
                        Leaderboard();
                    }
                    else if (open == "Exit" || open == "3")
                    {
                        
                    }
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        
        public static void Game()
        {
            
        }

        public static void Leaderboard()
        {

        }

        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("MultiThreadedEchoServer started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Accepted new client connection...");
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}