﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;


namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {
                
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                int num = 1;
                String c = String.Empty;

                string name = reader.ReadLine();
                Console.WriteLine("Client Name : {0}", name);

                string open = reader.ReadLine();
                Console.WriteLine("Your Option : {0}", open);

                while (true)
                {
                    string run = reader.ReadLine();
                    Console.WriteLine("Client : {0}", run);

                    int score = 0;
                    if (open == "Play" || open == "1")
                    {

                    }
                    else if (open == "Exit" || open == "2")
                    {
                        client.Close();
                    }
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        
        /*public static void Game(out int input)
        {
            Random random = new Random();
            int input1 = random.Next(0, 9);
            int input2 = random.Next(0, 9);
            int input3 = random.Next(0, 9);
            int input4 = random.Next(0, 9);

            Console.WriteLine("{0} - {0} - {0} - {0}", input1, input2, input3, input4);

            //Console.WriteLine("{0} - ", input2);
            //Console.WriteLine("{0} - ", input3);
            //Console.WriteLine("{0} - ", input4);
        }
        */
        public static void Leaderboard()
        {

        }

        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("MultiThreadedEchoServer started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Accepted new client connection...");
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}