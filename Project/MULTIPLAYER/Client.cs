﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;

            Console.WriteLine("====================");
            Console.WriteLine("Welcome to the Game");
            Console.WriteLine("====================");

            Console.Write("Your Name : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            Console.WriteLine("1. Play");
            writer.Flush();
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();
            
            Console.WriteLine("2. Exit");
            writer.Flush();
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            Console.Write("Option : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            int score = 0;

            while (true)
            {
                Random random = new Random();
                int input1 = random.Next(0, 9);
                int input2 = random.Next(0, 9);
                int input3 = random.Next(0, 9);
                int input4 = random.Next(0, 9);

                Console.WriteLine(input1 + " - " + input2 + " - " + input3 + " - " + input4 + "\nScore : " + score);
                writer.WriteLine(input1 + " - " + input2 + " - " + input3 + " - " + input4 + "\nScore : " + score);
                writer.Flush();

                Console.WriteLine("Run : ");
                Console.ReadLine();

                if (input1 == run && run == input2 && run == input3 && run == input4)
                {
                    score += 10;
                    while (score == 100)
                    {
                        writer.WriteLine("You Win");
                        writer.Flush();
                    }
                }

                Console.Write("\n");
                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();
            }
            reader.Close();
            writer.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}