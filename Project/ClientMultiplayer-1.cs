﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;

            Console.WriteLine("====================");
            Console.WriteLine("Welcome to the Game");
            Console.WriteLine("====================");

            Console.Write("Your Name : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            while (true)
            {
                Console.Write("Option : ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("\n");
                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();
            }
            reader.Close();
            writer.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}