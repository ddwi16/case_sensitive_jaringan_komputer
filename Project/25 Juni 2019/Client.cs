﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

public class EchoClient
{
    public static void Main(string[] args)
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);

            String s = String.Empty;
            String run = String.Empty;

            Thread thread = new Thread(o => RecvData((TcpClient)o));

            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());

            Console.WriteLine("===================================================");
            Console.WriteLine("============ << Welcome to the Game >> ============");
            Console.WriteLine("================= << Lets Run! >> =================");
            Console.WriteLine("===================================================");

            Console.Write("\nYour Name : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            Console.WriteLine("\nThis is MainMenu \n" +
                "1. Play \n" +
                "2. Exit \n");
            Console.WriteLine("Attention :" +
                "\n 1. You can't run alone toward your life, you can invite your EX!" +
                "\n 2. This Game need 2 players until 4 for max players (*,*)" +
                "\n 3. Enjoy this Game, don't be like SadBoy!" +
                "\n 4. IF you not read this, ERROR IS WATCHING YOU!");
            writer.Flush();

            Console.Write("\n Option : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            while (true)
            {
                Console.Write("\n");
                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();
                
                Console.Write("Run1: ");
                run = Console.ReadLine();
                writer.WriteLine(run);
                //writer.Flush();

                Console.Write("Run2: ");
                run = Console.ReadLine();
                writer.WriteLine(run);
                //writer.Flush();

                Console.Write("Run3: ");
                run = Console.ReadLine();
                writer.WriteLine(run);
                //writer.Flush();

                Console.Write("Run4: ");
                run = Console.ReadLine();
                writer.WriteLine(run);
                //writer.Flush();

                bool input = string.IsNullOrEmpty(run);

                if (input == true)
                {
                    Console.Clear();
                    Console.WriteLine(" Are you serious?\n" +
                        " Stay focus pal!");
                    for (int a = 5; a >= 0; a--)
                    {
                        Console.SetCursorPosition(0, 2);
                        Console.WriteLine("This penalty for you {0}", a);
                        Thread.Sleep(1000);
                        continue;
                    }
                    Console.Clear();
                    String option = String.Empty;
                    Console.WriteLine("You are Lose, cause not running");
                    Console.WriteLine("Want to play again? \n" +
                        "(Y / N)\n");
                    Console.WriteLine(" Option: ");
                    option = Console.ReadLine();
                    writer.WriteLine(option);
                    writer.Flush();
                    continue;
                }
                else if (server_string == "You Win")
                {
                    System.Threading.Thread.Sleep(10000);
                    Console.WriteLine(server_string);
                    client.Close();
                }
                else
                {
                    writer.Flush();
                    Console.Clear();
                    continue;
                }
                
                if(int.Parse(server_string) == 100)
                {
                    thread.Start(client);
                    for (int a = 10; a >= 0; a--)
                    {
                        Console.SetCursorPosition(0, 2);
                        Console.WriteLine("Thank You for playing\n" +
                            "You will be log out in {0}", a);
                        Thread.Sleep(1000);
                        client.Close();
                    }
                }
            }
            thread.Join();
            reader.Close();
            writer.Close();
            client.Close();
            Console.ReadKey();
            //client.Client.Shutdown(SocketShutdown.Send);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    static void RecvData(TcpClient client)
    {
        NetworkStream netStr = client.GetStream();
        byte[] receivedBytes = new byte[1024];
        int byteCount;

        while((byteCount = netStr.Read(receivedBytes, 0, receivedBytes.Length)) > 0)
        {
            Console.Write(Encoding.ASCII.GetString(receivedBytes, 0, byteCount));
        }
    }
}