﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        static readonly object locked = new object();
        static readonly Dictionary<int, TcpClient> listClients = new Dictionary<int, TcpClient>();

        public static void Main()
        {
            //Socket socket = new Socket;
            int count = 1;
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("Server Started . . .");
                while (true)
                {
                    Console.WriteLine("Waiting . . .");
                    
                    TcpClient client = listener.AcceptTcpClient();
                    lock (locked) listClients.Add(count, client);
                    Console.WriteLine("Accepted" +
                        " >> Client no: " + Convert.ToString(count) + " Connected");

                    Thread t = new Thread(Game);
                    t.Start(count);
                    count++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    

        private static void Game(object argument)
        {
            int id = (int)argument;
            TcpClient client;

            lock (locked) client = listClients[id];

            try
            {
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                String c = String.Empty;
                string name = reader.ReadLine();
                Console.WriteLine("Client Name : {0}", name);
                string open = reader.ReadLine();
                int score = 0;

                while (true)
                {
                    if (open == "Play" || open == "1" || open == "play")
                    {
                        Random random = new Random();
                        int input1 = random.Next(0, 9);
                        int input2 = random.Next(0, 9);
                        int input3 = random.Next(0, 9);
                        int input4 = random.Next(0, 9);

                        string a = score.ToString();
                        writer.WriteLine(input1 + " - " + input2 + " - " + input3 + " - " + input4 + " Distance : " + score);
                        writer.Flush();

                        string inp1 = reader.ReadLine();
                        string inp2 = reader.ReadLine();
                        string inp3 = reader.ReadLine();
                        string inp4 = reader.ReadLine();

                        bool i1 = string.IsNullOrEmpty(inp1);
                        bool i2 = string.IsNullOrEmpty(inp2);
                        bool i3 = string.IsNullOrEmpty(inp3);
                        bool i4 = string.IsNullOrEmpty(inp4);

                        if (i1 == true || i2 == true || i3 == true || i4 == true)
                        {
                            String opt = String.Empty;
                            string option = reader.ReadLine();

                            if (option == "Y" || option == "y")
                            {
                                continue;
                            }
                            else if(option == "N" || option == "n")
                            {
                                client.Close();
                            }
                        }

                        else if (int.Parse(inp1) == input1 && int.Parse(inp2) == input2 && int.Parse(inp3) == input3 && int.Parse(inp4) == input4)
                        {
                            score += 10;
                            if (score == 100)
                            {
                                score = 100;
                                //broadcast
                                broadcast(name + " You Win\n");
                                writer.WriteLine(score);
                                writer.Flush();
                                writer.WriteLine(" Do you want to play again? \n" +
                                        " 1. Play \n" +
                                        " 2. Exit \n");
                                writer.Flush();
                                String ag = String.Empty;
                                string again = reader.ReadLine();

                                client.Close();
                            }
                        }
                    }
                    else if (open == "Exit" && open == "2")
                    {
                        client.Close();
                    }
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }

        public static void broadcast(string data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

            lock (locked)
            {
                foreach(TcpClient client in listClients.Values)

                {
                    NetworkStream stream = client.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
    }
}