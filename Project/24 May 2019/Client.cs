﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;
            String run = String.Empty;

            Console.WriteLine("====================");
            Console.WriteLine("Welcome to the Game");
            Console.WriteLine("====================");

            Console.Write("Your Name : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            Console.WriteLine("1. Play\n2. Exit\n");
            writer.Flush();

            Console.Write("Option : ");
            s = Console.ReadLine();
            writer.WriteLine(s);
            writer.Flush();

            int score = 0;

            while (true)
            {
                Console.Write("\n");
                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();

                Console.Write("Run1 : ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Run2: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Run3: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Run4: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();
                
            }
            reader.Close();
            writer.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}